package idus.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Service;

import idus.model.request.UserRequest;
import idus.model.response.UserDetailResponse;
import idus.model.response.UserResponse;
import idus.repository.UserRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserRepository repository;

    public UserResponse register(UserRequest user){
        
    	UserResponse response = repository.register(user);
    	
    	return response;
    }

    public UserResponse login(UserRequest user, HttpServletRequest req){
    	
    	UserResponse response = repository.login(user, req);
    	
        return response;
    }
    
    public UserDetailResponse userinfo(String id){
    	
    	UserDetailResponse response = repository.userinfo(id);
    	
    	return response;
    }
}
