package idus.service;

import java.util.List;

import org.springframework.stereotype.Service;

import idus.model.request.UserRequest;
import idus.model.response.OrderResponse;
import idus.model.response.UserResponse;
import idus.repository.OrderRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class OrderService {
    private final OrderRepository repository;

    public List<OrderResponse> list(UserRequest user){
        
    	List<OrderResponse> response = repository.list(user);
    	
    	return response;
    }
  
}
