package idus.repository;

import javax.servlet.http.HttpServletRequest;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import idus.model.request.UserRequest;
import idus.model.response.UserDetailResponse;
import idus.model.response.UserResponse;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class UserRepository {
    private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    public UserResponse register(UserRequest user) {
    	int r = 0;
    	String result = "";
    	try {
	    	String query = "INSERT INTO user_table(name, nickname, password , tel, email, gender) values (:name, :nickname, :password , :tel, :email, :gender)";
	    	
	    	
	    	
	    	if(!user.getName().matches("^[ㄱ-ㅎ가-힣a-zA-Z]*$")) {	    	
	    		result = "한글, 영문 대소문자만 허용";
	    		return UserResponse.builder().result(result).build(); 
	    	}
	    	
	    	if(!user.getNickname().matches("^[a-z]*$")) {
	    		result = "영문 소문자만 허용";
	    		return UserResponse.builder().result(result).build(); 
	    	}
	    	
	    	if(!user.getPassword().matches("^(?=.*[a-zA-Z])((?=.*\\d))((?=.+[~!@#$%^&*()_+|<>?:{}])).{10,}$")) {
	    		result = "영문 대문자, 영문 소문자, 특수 문자, 숫자 각 1개 이상씩 포함 및 10자이상";
	    		return UserResponse.builder().result(result).build(); 
	    	}
	    	
	    	if(!user.getTel().matches("^[0-9]+$")) {
	    		result = "숫자만 허용";
	    		return UserResponse.builder().result(result).build(); 
	    	}
	    	
	    	if(!user.getEmail().matches("^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$")) {
	    		result = "이메일형식만  허용";
	    		return UserResponse.builder().result(result).build(); 
	    	}
	    	
	    	
	    	
	    	
	    	MapSqlParameterSource params = new MapSqlParameterSource();
	    	params.addValue("name", user.getName());
	    	params.addValue("nickname", user.getNickname());
	    	params.addValue("password", user.getPassword());
	    	params.addValue("tel", user.getTel());
	    	params.addValue("email", user.getEmail());
	    	params.addValue("gender", user.getGender());
			r = namedParameterJdbcTemplate.update(query,params);
    	}catch (Exception e) {
			e.printStackTrace(); 
		}
    	
    	if(r > 0) {
    		result = "성공";
    	}else {
    		result = "실패";
    	}
    	
		return UserResponse.builder().result(result).build(); 
    			
    }
    
    
    public UserResponse login(UserRequest user , HttpServletRequest req) {
    	UserRequest r = null;
    	String result = "";
    	try {
	    	String query = "select nickname from user_table WHERE name = :name and password = :password";
	    	
	    	
	    	MapSqlParameterSource params = new MapSqlParameterSource();
	    	params.addValue("name", user.getName());
	    	params.addValue("password", user.getPassword());
			r = namedParameterJdbcTemplate.queryForObject(
						query,
						params,
						(rs, rowNum) -> UserRequest.builder().nickname(rs.getString("nickname")).build()
			);
    	}catch (Exception e) {
			e.printStackTrace(); 
		}
    	
    	if(r != null && StringUtils.isNotEmpty(r.getNickname())) {
    		result = r.getNickname()  + "님이 로그인 되었습니다.";    	
    		req.getSession().setAttribute(user.getName(), this);
    	}else {
    		result = "ID 혹은 패스워드가 다릅니다.";
    	}
    	
		return UserResponse.builder().result(result).build(); 
    			
    }
    
    public UserDetailResponse userinfo(String id) {
    	try {
    		String query = "select nickname, tel, email, gender from user_table WHERE name = :name";
    		
    		
    		MapSqlParameterSource params = new MapSqlParameterSource();
    		params.addValue("name", id);    		
    		return namedParameterJdbcTemplate.queryForObject(
    				query,
    				params,
    				(rs, rowNum) -> UserDetailResponse.builder()
    					.nickname(rs.getString("nickname"))
    					.tel(rs.getString("tel"))
    					.email(rs.getString("email"))
    					.gender(rs.getString("gender"))
    					.build()
    				);
    	}catch (Exception e) {
    		e.printStackTrace(); 
    	}
    	
    	return null;
    }
}
