package idus.repository;

import java.util.List;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import idus.model.request.UserRequest;
import idus.model.response.OrderResponse;
import idus.model.response.UserDetailResponse;
import idus.model.response.UserResponse;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Repository
public class OrderRepository {
	 private final NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	 
	 
	 public List<OrderResponse> list(UserRequest user) {
	    	int r = 0;
	    	String result = "";
	    	try {
		    	String query = "SELECT orl.name , ot.order_no , ot.product_name , ot.pay_datetime from order_relation orl left join order_table ot on orl.order_no = ot.order_no where orl.name = :name  ";
		    	
		    	
		    	MapSqlParameterSource params = new MapSqlParameterSource();
		    	params.addValue("name", user.getName());		    	
		    	return namedParameterJdbcTemplate.query(
	    				query,
	    				params,
	    				(rs, rowNum) -> OrderResponse.builder()
	    					.name(rs.getString("name"))
	    					.order_no(rs.getString("order_no"))
	    					.product_name(rs.getString("product_name"))
	    					.pay_datetime(rs.getDate("pay_datetime"))
	    					.build()
	    				);
	    	}catch (Exception e) {
				e.printStackTrace(); 
			}
	    	
			return null; 
	    			
	    }
}
