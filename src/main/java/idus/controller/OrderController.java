package idus.controller;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idus.model.request.UserRequest;
import idus.model.response.OrderResponse;
import idus.model.response.UserResponse;
import idus.service.OrderService;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/order")
public class OrderController {

    private final OrderService service;

    //주문목록
    @PostMapping("/list")
    public ResponseEntity<List<OrderResponse>> list(@RequestBody UserRequest user) {
       List<OrderResponse> response = service.list(user);

       return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }
   
}
