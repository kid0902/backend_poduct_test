package idus.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import idus.model.request.UserRequest;
import idus.model.response.UserDetailResponse;
import idus.model.response.UserResponse;
import idus.service.UserService;
import io.micrometer.core.instrument.util.StringUtils;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserService service;

    //회원가입
    @PostMapping("/register")
    public ResponseEntity<UserResponse> register(@RequestBody UserRequest user) {
       UserResponse response = service.register(user);

       return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }
    
    @PostMapping("/login")
    public ResponseEntity<UserResponse> login(@RequestBody UserRequest user , HttpServletRequest req) {
    	
    	String chk = (String)req.getSession().getAttribute("userSession");
    	UserResponse response = null;
    	
    	System.out.println(chk);
    	
    	if(StringUtils.isNotEmpty(chk) && chk.equals(user.getName())) {
    		response = UserResponse.builder().result("로그인되어있음").build();
    	}else {    	
    		response = service.login(user, req);
    	}
    	
    	return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }

    @GetMapping("/logout/{id}")
    public ResponseEntity<UserResponse> logout(@PathVariable("id") String id , HttpServletRequest req) {
    	
    	req.getSession().removeAttribute(id);
    	
    	UserResponse response = UserResponse.builder().result("로그아웃됨").build();
    	
    	return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }
    
    @GetMapping("/userinfo/{id}")
    public ResponseEntity<UserDetailResponse> userinfo(@PathVariable("id") String id) {
    	
    	UserDetailResponse response = service.userinfo(id);
    	
    	return new ResponseEntity<>(response, HttpStatus.OK);
    	
    }
}
