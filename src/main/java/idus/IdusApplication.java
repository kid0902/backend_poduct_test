package idus;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class IdusApplication {

    public static void main(String[] args) {
        SpringApplication.run(IdusApplication.class, args);
    }

}
