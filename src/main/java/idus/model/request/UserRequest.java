package idus.model.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserRequest {
	
    private String name;
	
    private String nickname;
	
    private String password;
	
    private String tel;
	
    private String email;
	
    private String gender;
     
	
	@JsonCreator
	public UserRequest(@JsonProperty("name") String name , @JsonProperty("nickname") String nickname ,  @JsonProperty("password")String password ,
			@JsonProperty("tel") String tel , @JsonProperty("email") String email , @JsonProperty("gender") String gender) {
		this.name = name;
		this.nickname = nickname;
		this.password = password;
		this.tel = tel;
		this.email = email;
		this.gender = gender;

	}



}
