package idus.model.request;

import java.util.Date;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OrderRequest {
	private String order_no;
	private String product_name;
	private Date pay_datetime;
	private String name;

}
