DROP TABLE user_table IF EXISTS;

CREATE TABLE user_table
(
	  name varchar(20) not null
	, nickname varchar(30) not null
	, password varchar(255) not null
	, tel varchar(20) not null
	, email varchar(100) not null
	, gender varchar(2) null
);


DROP TABLE order_table IF EXISTS;

CREATE TABLE order_table
(
	  order_no varchar(12) NOT NULL
	, product_name varchar(100) NOT NULL
	, pay_datetime Datetime NOT NULL
);

DROP TABLE order_relation IF EXISTS;

CREATE TABLE order_relation(
	  name varchar(20) not null
	, order_no varchar(12) NOT NULL
);